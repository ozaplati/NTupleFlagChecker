The lar, tile, sct detector flags were implemented in v9 version of NTuples.
You should use second constructor for earlier version of NTuples:
``` 
...
bool _doFlags = false;
FlagChecker(TTree *tree, bool _doFlags);
```



# Example 1
 - run in root-terminal
    - only one input ROOT-File

```
# run root env.
root -l /eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data17_STDM11/v9/EMTopoJets_100k_test/data-ANALYSIS/data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM11.grp23_v01_p4173.root
  
  # Load the class of FlagChecker
  .L FlagChecker.C
  
  # Event loop
  FlagChecker t
  t.Loop()
```

# Example 2
 - run in root-terminal
    - on multiple ROOT-Files in using ROOTFileList
    - ROOT-File list - as a list of input ROOT-Files - use convention - one ROOT-File per one line
    - based on TChain 

```
root -l 
  
  # Load the class
  .L FlagChecker.C
  
  # setup input ROOTFileList
  string _ROOTFileList = "ROOTFileLists/Data17_v9_100kEvents_test.txt"
  
  # ROOTFileList to TChain
  TFileCollection fc("TFileCollection","TFileCollection",_ROOTFileList.c_str(), -1, 1)
  string tDirectory = "TinyJetTrees_IJXS"
  TChain *chain = new TChain(tDirectory.c_str())
  chain->AddFileInfoList((TCollection*) fc.GetList())
  
  # Event loop
  FlagChecker t(chain)
  t.Loop()
```



# Example 3
 - run in root-terminal
    - on multiple ROOT-Files using ROOTFileList
    - based on TChain
    - based on `macro.C`
   
```
root -l 2>&1 | tee <OUTPUT_FILE>
  
  # Load the class
  .L FlagChecker.C
  .L macro.C
  
  # execute a macro
  macro_Data<XX>_v<YY>()
  
```
