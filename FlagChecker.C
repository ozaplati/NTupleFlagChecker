#define FlagChecker_cxx
#include "FlagChecker.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>

void FlagChecker::Loop()
{
//   In a ROOT session, you can do:
//      root> .L FlagChecker.C
//      root> FlagChecker t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();
   

   
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) 
   {
      Long64_t ientry = LoadTree(jentry);
      
      if (ientry < 0) break;
      
      /// switch on all branches 
      //nb = fChain->GetEntry(jentry);   nbytes += nb;
      
      /// switch on only selected branches
      if (b_doFlags == true)
      {
          b_larFlags->GetEntry(ientry);
          b_tileFlags->GetEntry(ientry);
          b_sctFlags->GetEntry(ientry);
          b_coreFlags->GetEntry(ientry);
          
          b_lumiBlock->GetEntry(ientry);
          b_eventNumber->GetEntry(ientry);
          b_RunNumber->GetEntry(ientry);
          
          b_jet_pt->GetEntry(ientry);
       }
       else
       {
           b_jet_pt->GetEntry(ientry);
       }
       
      /// Event Print
      if (N_event % 100000 == 0)
      {
          std::cout << "Passed Events: " << N_event << std::endl;
      }
      
      /// event counter
      N_event += 1;
      
      /// detector flag prints
      if (b_doFlags == true)
      {
          
          if (larFlags  != 0) N_larFlag  +=1;
          if (tileFlags != 0) N_tileFlag +=1;
          if (sctFlags  != 0) N_sctFlag  +=1;
          if (coreFlags != 0) N_coreFlag +=1;
          
          if (larFlags != 0 || tileFlags != 0 || sctFlags != 0 || coreFlags  != 0)
          {
               N_DetFlags += 1;
               std::cout << "lumiBlock: " << lumiBlock << "\t RunNumber: " << RunNumber << "\t eventNumber: " << eventNumber << "\t larFlags: " << larFlags << "\t tileFlags: " << tileFlags << "\t sctFlags: " << sctFlags << "\t coreFlags: " << coreFlags << "\t NJets:" << jet_pt->size() << "\t N_DetFlags: " << N_DetFlags << "\t N_larFlag: " << N_larFlag << "\t N_tileFlag: " << N_tileFlag << "\t N_sctFlag: " << N_sctFlag << "\t N_coreFlag: " << N_coreFlag << std::endl;
          }
      }
      // if (Cut(ientry) < 0) continue;
   }
   
   /// Summary at the end of the LOOP
   Long64_t N_event_chain = fChain->GetEntriesFast();
   
   /// Summary - totoal number of events
   std::cout << "\n\n\n" << std::endl;
   std::cout << "summary" << std::endl;
   std::cout << "Total number of event in TChain:            " << N_event_chain << std::endl;
   std::cout << "Total number of event in LOOP:              " << N_event << std::endl;
   
   /// Summary for detesctor flags
   if (b_doFlags == true)
   {
       /// detector all flags - lar & tile & sct & core
       std::cout << "\n\n" << std::endl;
       std::cout << "N_DetFlags:                                 " << N_DetFlags << std::endl;
       std::cout << "N_DetFlags/Total number of event in TChain: " << N_DetFlags / (N_event_chain * 1.0)<< std::endl;
       std::cout << "N_DetFlags/Total number of event in LOOP:   " << N_DetFlags / (N_event * 1.0)<< std::endl;
       
       /// only lar
       std::cout << "\n\n" << std::endl;
       std::cout << "N_larFlag:                                  " << N_larFlag << std::endl;
       std::cout << "N_larFlag/N_DetFlags:                       " << N_larFlag / (N_DetFlags * 1.0)<< std::endl;
       std::cout << "N_larFlag/Total number of event in TChain:  " << N_larFlag / (N_event_chain * 1.0)<< std::endl;
       std::cout << "N_larFlag/Total number of event in LOOP:    " << N_larFlag / (N_event * 1.0)<< std::endl;
       
       /// only tile
       std::cout << "\n\n" << std::endl;
       std::cout << "N_tileFlag:                                 " << N_tileFlag << std::endl;
       std::cout << "N_larFlag/N_DetFlags:                       " << N_tileFlag / (N_DetFlags * 1.0)<< std::endl;
       std::cout << "N_tileFlag/Total number of event in TChain: " << N_tileFlag / (N_event_chain * 1.0)<< std::endl;
       std::cout << "N_tileFlag/Total number of event in LOOP:   " << N_tileFlag / (N_event * 1.0)<< std::endl;
       
       /// only sct
       std::cout << "\n\n" << std::endl;
       std::cout << "N_sctFlag:                                  " << N_sctFlag << std::endl;
       std::cout << "N_sctFlag/N_DetFlags:                       " << N_sctFlag / (N_DetFlags * 1.0)<< std::endl;
       std::cout << "N_sctFlag/Total number of event in TChain:  " << N_sctFlag / (N_event_chain * 1.0)<< std::endl;
       std::cout << "N_sctFlag/Total number of event in LOOP:    " << N_sctFlag / (N_event * 1.0)<< std::endl;
    
       /// only core
       std::cout << "\n\n" << std::endl;
       std::cout << "N_coreFlag:                                  " << N_coreFlag << std::endl;
       std::cout << "N_coreFlag/N_DetFlags:                       " << N_coreFlag / (N_DetFlags * 1.0)<< std::endl;
       std::cout << "N_coreFlag/Total number of event in TChain:  " << N_coreFlag / (N_event_chain * 1.0)<< std::endl;
       std::cout << "N_coreFlag/Total number of event in LOOP:    " << N_coreFlag / (N_event * 1.0)<< std::endl;
    }
  std::cout << "\n\n" << std::endl;
  std::cout << "DONE!" << std::endl;
}
