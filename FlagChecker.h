//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Oct 20 10:38:34 2020 by ROOT version 6.22/02
// from TTree TinyJetTrees_IJXS/tiny jet trees
// found on file: data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM11.grp23_v01_p4173.root
//////////////////////////////////////////////////////////

#ifndef FlagChecker_h
#define FlagChecker_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

using namespace std;

class FlagChecker {
public :

   Long64_t N_DetFlags = 0;
   Long64_t N_event = 0;
   Long64_t N_larFlag = 0;
   Long64_t N_tileFlag = 0;
   Long64_t N_sctFlag = 0;
   Long64_t N_coreFlag = 0;
   
   bool b_doFlags = true;
   
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         mcWeight;
   vector<float>   *showerWeights;
   vector<float>   *pdfWeights;
   Float_t         weight_pileup;
   Float_t         weight_pileup_up;
   Float_t         weight_pileup_down;
   Float_t         weight_JVT_SF;
   Float_t         weight_JVT_SF_UP;
   Float_t         weight_JVT_SF_DOWN;
   Float_t         weight_fJVT_SF;
   Float_t         weight_fJVT_SF_UP;
   Float_t         weight_fJVT_SF_DOWN;
   Int_t           mcChannelNumber;
   Int_t           RunNumber;
   Long64_t        eventNumber;
   Int_t           mcEventNumber;
   Int_t           lumiBlock;
   Int_t           coreFlags;
   Int_t           larFlags;
   Int_t           tileFlags;
   Int_t           sctFlags;
   Int_t           bcid;
   Int_t           DistEmptyBCID;
   Int_t           DistLastUnpairedBCID;
   Int_t           DistNextUnpairedBCID;
   Int_t           DistFromFrontBCID;
   Float_t         mu;
   Int_t           npv;
   Float_t         pv_z;
   Int_t           passL1;
   Int_t           passHLT;
   vector<string>  *passedTriggers;
   vector<string>  *disabledTriggers;
   vector<float>   *triggerPrescales;
   vector<string>  *isPassBitsNames;
   vector<unsigned int> *isPassBits;
   Bool_t          eventClean_LooseBad;
   Bool_t          isBatBatmanFlag;
   Int_t           ntruthJet;
   vector<float>   *truthJet_pt;
   vector<float>   *truthJet_eta;
   vector<float>   *truthJet_phi;
   vector<float>   *truthJet_E;
   vector<float>   *truthJet_Ntrk;
   Int_t           nhltjet;
   vector<float>   *hltjet_pt;
   vector<float>   *hltjet_eta;
   vector<float>   *hltjet_phi;
   vector<float>   *hltjet_E;
   Int_t           njet;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_E;
   vector<float>   *jet_JVT;
   vector<int>     *jet_PassJVT;
   vector<int>     *jet_PassfJVT;
   vector<int>     *jetNtrk;
   vector<int>     *jet_PartonTruthLabelID;
   vector<int>     *jet_TileStatus;
   vector<int>     *jet_CleanTightBad;
   vector<int>     *jet_CleanLooseBad;
   vector<vector<float> > *jet_JESJER_pt;
   vector<vector<float> > *jet_JESJER_eta;
   vector<vector<float> > *jet_JESJER_phi;
   vector<vector<float> > *jet_JESJER_E;
   vector<float>   *jetChargedFrac;
   vector<float>   *jetTile0;
   vector<float>   *jetEM3;
   vector<float>   *jetWtrk;
   vector<vector<float> > *jet_EnergyPerSampling;
   vector<float>   *jetTiming;
   vector<float>   *jetLArQuality;
   vector<float>   *jetHECQuality;
   vector<float>   *jetNegativeE;
   vector<float>   *jetAverageLArQF;
   vector<float>   *jetHECFrac;
   vector<float>   *jetEMFrac;
   vector<float>   *jetFracSamplingMax;
   vector<int>     *jetFracSamplingMaxIndex;

   // List of branches
   TBranch        *b_mcWeight;   //!
   TBranch        *b_showerWeights;   //!
   TBranch        *b_pdfWeights;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_pileup_up;   //!
   TBranch        *b_weight_pileup_down;   //!
   TBranch        *b_weight_JVT_SF;   //!
   TBranch        *b_weight_JVT_SF_UP;   //!
   TBranch        *b_weight_JVT_SF_DOWN;   //!
   TBranch        *b_weight_fJVT_SF;   //!
   TBranch        *b_weight_fJVT_SF_UP;   //!
   TBranch        *b_weight_fJVT_SF_DOWN;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_mcEventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_DistEmptyBCID;   //!
   TBranch        *b_DistLastUnpairedBCID;   //!
   TBranch        *b_DistNextUnpairedBCID;   //!
   TBranch        *b_DistFromFrontBCID;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_npv;   //!
   TBranch        *b_pv_z;   //!
   TBranch        *b_passL1;   //!
   TBranch        *b_passHLT;   //!
   TBranch        *b_passedTriggers;   //!
   TBranch        *b_disabledTriggers;   //!
   TBranch        *b_triggerPrescales;   //!
   TBranch        *b_isPassBitsNames;   //!
   TBranch        *b_isPassBits;   //!
   TBranch        *b_eventClean_LooseBad;   //!
   TBranch        *b_isBatBatmanFlag;   //!
   TBranch        *b_ntruthJet;   //!
   TBranch        *b_truthJet_pt;   //!
   TBranch        *b_truthJet_eta;   //!
   TBranch        *b_truthJet_phi;   //!
   TBranch        *b_truthJet_E;   //!
   TBranch        *b_truthJet_Ntrk;   //!
   TBranch        *b_nhltjet;   //!
   TBranch        *b_hltjet_pt;   //!
   TBranch        *b_hltjet_eta;   //!
   TBranch        *b_hltjet_phi;   //!
   TBranch        *b_hltjet_E;   //!
   TBranch        *b_njet;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_jet_JVT;   //!
   TBranch        *b_jet_PassJVT;   //!
   TBranch        *b_jet_PassfJVT;   //!
   TBranch        *b_jetNtrk;   //!
   TBranch        *b_jet_PartonTruthLabelID;   //!
   TBranch        *b_jet_TileStatus;   //!
   TBranch        *b_jet_CleanTightBad;   //!
   TBranch        *b_jet_CleanLooseBad;   //!
   TBranch        *b_jet_JESJER_pt;   //!
   TBranch        *b_jet_JESJER_eta;   //!
   TBranch        *b_jet_JESJER_phi;   //!
   TBranch        *b_jet_JESJER_E;   //!
   TBranch        *b_jetChargedFrac;   //!
   TBranch        *b_jetTile0;   //!
   TBranch        *b_jetEM3;   //!
   TBranch        *b_jetWtrk;   //!
   TBranch        *b_jet_EnergyPerSampling;   //!
   TBranch        *b_jetTiming;   //!
   TBranch        *b_jetLArQuality;   //!
   TBranch        *b_jetHECQuality;   //!
   TBranch        *b_jetNegativeE;   //!
   TBranch        *b_jetAverageLArQF;   //!
   TBranch        *b_jetHECFrac;   //!
   TBranch        *b_jetEMFrac;   //!
   TBranch        *b_jetFracSamplingMax;   //!
   TBranch        *b_jetFracSamplingMaxIndex;   //!

   FlagChecker(TTree *tree=0);
   FlagChecker(TTree *tree=0, bool _doFlags=true);

   virtual ~FlagChecker();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef FlagChecker_cxx
FlagChecker::FlagChecker(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM11.grp23_v01_p4173.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM11.grp23_v01_p4173.root");
      }
      f->GetObject("TinyJetTrees_IJXS",tree);

   }
   Init(tree);
   
   b_doFlags = true;
}

FlagChecker::FlagChecker(TTree *tree=0, bool _doFlags): fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM11.grp23_v01_p4173.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM11.grp23_v01_p4173.root");
      }
      f->GetObject("TinyJetTrees_IJXS",tree);

   }
   Init(tree);
   
   b_doFlags = _doFlags;
}


FlagChecker::~FlagChecker()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t FlagChecker::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t FlagChecker::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void FlagChecker::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   showerWeights = 0;
   pdfWeights = 0;
   passedTriggers = 0;
   disabledTriggers = 0;
   triggerPrescales = 0;
   isPassBitsNames = 0;
   isPassBits = 0;
   truthJet_pt = 0;
   truthJet_eta = 0;
   truthJet_phi = 0;
   truthJet_E = 0;
   truthJet_Ntrk = 0;
   hltjet_pt = 0;
   hltjet_eta = 0;
   hltjet_phi = 0;
   hltjet_E = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_E = 0;
   jet_JVT = 0;
   jet_PassJVT = 0;
   jet_PassfJVT = 0;
   jetNtrk = 0;
   jet_PartonTruthLabelID = 0;
   jet_TileStatus = 0;
   jet_CleanTightBad = 0;
   jet_CleanLooseBad = 0;
   jet_JESJER_pt = 0;
   jet_JESJER_eta = 0;
   jet_JESJER_phi = 0;
   jet_JESJER_E = 0;
   jetChargedFrac = 0;
   jetTile0 = 0;
   jetEM3 = 0;
   jetWtrk = 0;
   jet_EnergyPerSampling = 0;
   jetTiming = 0;
   jetLArQuality = 0;
   jetHECQuality = 0;
   jetNegativeE = 0;
   jetAverageLArQF = 0;
   jetHECFrac = 0;
   jetEMFrac = 0;
   jetFracSamplingMax = 0;
   jetFracSamplingMaxIndex = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mcWeight", &mcWeight, &b_mcWeight);
   fChain->SetBranchAddress("showerWeights", &showerWeights, &b_showerWeights);
   fChain->SetBranchAddress("pdfWeights", &pdfWeights, &b_pdfWeights);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_pileup_up", &weight_pileup_up, &b_weight_pileup_up);
   fChain->SetBranchAddress("weight_pileup_down", &weight_pileup_down, &b_weight_pileup_down);
   fChain->SetBranchAddress("weight_JVT_SF", &weight_JVT_SF, &b_weight_JVT_SF);
   fChain->SetBranchAddress("weight_JVT_SF_UP", &weight_JVT_SF_UP, &b_weight_JVT_SF_UP);
   fChain->SetBranchAddress("weight_JVT_SF_DOWN", &weight_JVT_SF_DOWN, &b_weight_JVT_SF_DOWN);
   fChain->SetBranchAddress("weight_fJVT_SF", &weight_fJVT_SF, &b_weight_fJVT_SF);
   fChain->SetBranchAddress("weight_fJVT_SF_UP", &weight_fJVT_SF_UP, &b_weight_fJVT_SF_UP);
   fChain->SetBranchAddress("weight_fJVT_SF_DOWN", &weight_fJVT_SF_DOWN, &b_weight_fJVT_SF_DOWN);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("mcEventNumber", &mcEventNumber, &b_mcEventNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("DistEmptyBCID", &DistEmptyBCID, &b_DistEmptyBCID);
   fChain->SetBranchAddress("DistLastUnpairedBCID", &DistLastUnpairedBCID, &b_DistLastUnpairedBCID);
   fChain->SetBranchAddress("DistNextUnpairedBCID", &DistNextUnpairedBCID, &b_DistNextUnpairedBCID);
   fChain->SetBranchAddress("DistFromFrontBCID", &DistFromFrontBCID, &b_DistFromFrontBCID);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("npv", &npv, &b_npv);
   fChain->SetBranchAddress("pv_z", &pv_z, &b_pv_z);
   fChain->SetBranchAddress("passL1", &passL1, &b_passL1);
   fChain->SetBranchAddress("passHLT", &passHLT, &b_passHLT);
   fChain->SetBranchAddress("passedTriggers", &passedTriggers, &b_passedTriggers);
   fChain->SetBranchAddress("disabledTriggers", &disabledTriggers, &b_disabledTriggers);
   fChain->SetBranchAddress("triggerPrescales", &triggerPrescales, &b_triggerPrescales);
   fChain->SetBranchAddress("isPassBitsNames", &isPassBitsNames, &b_isPassBitsNames);
   fChain->SetBranchAddress("isPassBits", &isPassBits, &b_isPassBits);
   fChain->SetBranchAddress("eventClean_LooseBad", &eventClean_LooseBad, &b_eventClean_LooseBad);
   fChain->SetBranchAddress("isBatBatmanFlag", &isBatBatmanFlag, &b_isBatBatmanFlag);
   fChain->SetBranchAddress("ntruthJet", &ntruthJet, &b_ntruthJet);
   fChain->SetBranchAddress("truthJet_pt", &truthJet_pt, &b_truthJet_pt);
   fChain->SetBranchAddress("truthJet_eta", &truthJet_eta, &b_truthJet_eta);
   fChain->SetBranchAddress("truthJet_phi", &truthJet_phi, &b_truthJet_phi);
   fChain->SetBranchAddress("truthJet_E", &truthJet_E, &b_truthJet_E);
   fChain->SetBranchAddress("truthJet_Ntrk", &truthJet_Ntrk, &b_truthJet_Ntrk);
   fChain->SetBranchAddress("nhltjet", &nhltjet, &b_nhltjet);
   fChain->SetBranchAddress("hltjet_pt", &hltjet_pt, &b_hltjet_pt);
   fChain->SetBranchAddress("hltjet_eta", &hltjet_eta, &b_hltjet_eta);
   fChain->SetBranchAddress("hltjet_phi", &hltjet_phi, &b_hltjet_phi);
   fChain->SetBranchAddress("hltjet_E", &hltjet_E, &b_hltjet_E);
   fChain->SetBranchAddress("njet", &njet, &b_njet);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("jet_JVT", &jet_JVT, &b_jet_JVT);
   fChain->SetBranchAddress("jet_PassJVT", &jet_PassJVT, &b_jet_PassJVT);
   fChain->SetBranchAddress("jet_PassfJVT", &jet_PassfJVT, &b_jet_PassfJVT);
   fChain->SetBranchAddress("jetNtrk", &jetNtrk, &b_jetNtrk);
   fChain->SetBranchAddress("jet_PartonTruthLabelID", &jet_PartonTruthLabelID, &b_jet_PartonTruthLabelID);
   fChain->SetBranchAddress("jet_TileStatus", &jet_TileStatus, &b_jet_TileStatus);
   fChain->SetBranchAddress("jet_CleanTightBad", &jet_CleanTightBad, &b_jet_CleanTightBad);
   fChain->SetBranchAddress("jet_CleanLooseBad", &jet_CleanLooseBad, &b_jet_CleanLooseBad);
   fChain->SetBranchAddress("jet_JESJER_pt", &jet_JESJER_pt, &b_jet_JESJER_pt);
   fChain->SetBranchAddress("jet_JESJER_eta", &jet_JESJER_eta, &b_jet_JESJER_eta);
   fChain->SetBranchAddress("jet_JESJER_phi", &jet_JESJER_phi, &b_jet_JESJER_phi);
   fChain->SetBranchAddress("jet_JESJER_E", &jet_JESJER_E, &b_jet_JESJER_E);
   fChain->SetBranchAddress("jetChargedFrac", &jetChargedFrac, &b_jetChargedFrac);
   fChain->SetBranchAddress("jetTile0", &jetTile0, &b_jetTile0);
   fChain->SetBranchAddress("jetEM3", &jetEM3, &b_jetEM3);
   fChain->SetBranchAddress("jetWtrk", &jetWtrk, &b_jetWtrk);
   fChain->SetBranchAddress("jet_EnergyPerSampling", &jet_EnergyPerSampling, &b_jet_EnergyPerSampling);
   fChain->SetBranchAddress("jetTiming", &jetTiming, &b_jetTiming);
   fChain->SetBranchAddress("jetLArQuality", &jetLArQuality, &b_jetLArQuality);
   fChain->SetBranchAddress("jetHECQuality", &jetHECQuality, &b_jetHECQuality);
   fChain->SetBranchAddress("jetNegativeE", &jetNegativeE, &b_jetNegativeE);
   fChain->SetBranchAddress("jetAverageLArQF", &jetAverageLArQF, &b_jetAverageLArQF);
   fChain->SetBranchAddress("jetHECFrac", &jetHECFrac, &b_jetHECFrac);
   fChain->SetBranchAddress("jetEMFrac", &jetEMFrac, &b_jetEMFrac);
   fChain->SetBranchAddress("jetFracSamplingMax", &jetFracSamplingMax, &b_jetFracSamplingMax);
   fChain->SetBranchAddress("jetFracSamplingMaxIndex", &jetFracSamplingMaxIndex, &b_jetFracSamplingMaxIndex);
   Notify();
}

Bool_t FlagChecker::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void FlagChecker::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t FlagChecker::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef FlagChecker_cxx
